package ru.t1.amsmirnov.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserRemoveByEmailRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserRemoveByEmailResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

@Component
public final class UserRemoveByEmailListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-remove-by-email";

    @NotNull
    public static final String DESCRIPTION = "Remove user by email.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@userRemoveByEmailListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRemoveByEmailRequest request = new UserRemoveByEmailRequest(getToken(), email);
        @NotNull final UserRemoveByEmailResponse response = userEndpoint.removeByEmail(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
