package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataJsonSaveFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataJsonSaveFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataJsonSaveFasterXMLListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-json";

    @NotNull
    public static final String DESCRIPTION = "Save data to JSON file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataJsonSaveFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXMLRequest request = new DataJsonSaveFasterXMLRequest(getToken());
        @NotNull final DataJsonSaveFasterXMLResponse response = domainEndpoint.saveDataJsonFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
