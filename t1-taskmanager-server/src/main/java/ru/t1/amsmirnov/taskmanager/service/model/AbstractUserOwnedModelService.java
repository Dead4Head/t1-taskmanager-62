package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.api.service.model.IUserOwnedService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.repository.model.AbstractUserOwnedRepository;
import ru.t1.amsmirnov.taskmanager.repository.model.UserRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractUserOwnedModelService<M extends AbstractUserOwnedModel, R extends AbstractUserOwnedRepository<M>>
        extends AbstractModelService<M, R>
        implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    protected UserRepository userRepository;

    public AbstractUserOwnedModelService(@NotNull final R repository) {
        super(repository);
    }

    @NotNull
    @Override
    @Transactional
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @Nullable final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        model.setUser(user.get());
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId, getComparator(comparator));
    }

    @NotNull
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> model = repository.findByUserIdAndId(userId, id);
        if (!model.isPresent()) throw new ModelNotFoundException();
        return model.get();
    }

    @NotNull
    @Override
    @Transactional
    public M removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> model = repository.findById(id);
        if (!model.isPresent()) throw new ModelNotFoundException();
        repository.delete(model.get());
        return model.get();
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    public long getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }

}
