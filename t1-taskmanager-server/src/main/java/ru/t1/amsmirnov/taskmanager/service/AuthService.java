package ru.t1.amsmirnov.taskmanager.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.service.IAuthService;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.ISessionDtoService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IUserDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.user.AccessDeniedException;
import ru.t1.amsmirnov.taskmanager.exception.util.CryptException;
import ru.t1.amsmirnov.taskmanager.util.CryptUtil;
import ru.t1.amsmirnov.taskmanager.util.HashUtil;

import java.util.Date;

@Service
public final class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISessionDtoService sessionService;

    @Nullable
    private String userId;

    public AuthService() {
    }

    private String getToken(@NotNull final SessionDTO session) throws JsonProcessingException, CryptException {
        @NotNull ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSession();
        return CryptUtil.encrypt(sessionKey, token);
    }

    private String getToken(@NotNull final UserDTO user) throws JsonProcessingException, AbstractException, CryptException {
        return getToken(createSession(user));
    }

    private SessionDTO createSession(@NotNull final UserDTO user) throws AbstractException {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        sessionService.add(session);
        return session;
    }


    @NotNull
    @Override
    public UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public String login(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        if (login == null || login.isEmpty())
            throw new LoginEmptyException();
        if (password == null || password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final UserDTO user;
        try {
            user = userService.findOneByLogin(login);
        } catch (final UserNotFoundException e) {
            throw new AccessDeniedException();
        }
        if (user.getPasswordHash() == null)
            throw new AccessDeniedException();
        if (user.isLocked())
            throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!user.getPasswordHash().equals(hash))
            throw new AccessDeniedException();
        userId = user.getId();
        @NotNull final String token;
        try {
            token = getToken(user);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException(e);
        }
        return token;
    }

    @Override
    public void logout(@Nullable final SessionDTO session) throws AbstractException {
        if (session == null) return;
        sessionService.removeOne(session);
    }

    @NotNull
    @Override
    public SessionDTO validateToken(@Nullable String token) throws AbstractException {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSession();
        @NotNull String json;
        @NotNull final SessionDTO session;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            session = objectMapper.readValue(json, SessionDTO.class);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getCreated();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

        return session;
    }

}
