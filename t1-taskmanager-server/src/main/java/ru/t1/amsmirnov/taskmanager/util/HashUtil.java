package ru.t1.amsmirnov.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.service.ISaltProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable final String value,
            @Nullable final String secret,
            @Nullable final Integer iteration
    ) {
        if (value == null) return null;
        if (iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String salt(
            @Nullable final ISaltProvider saltProvider,
            @Nullable final String value
    ) {
        if (saltProvider == null) return null;
        @Nullable final String secret = saltProvider.getPasswordSecret();
        @Nullable final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder stringBuilder = new StringBuilder();
            for (byte b : array) {
                stringBuilder.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuilder.toString();
        } catch (@NotNull NoSuchAlgorithmException exception) {
            exception.printStackTrace();
        }
        return null;
    }

}
