package ru.t1.amsmirnov.taskmanager.repository.dto;

import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;

public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> {

}
