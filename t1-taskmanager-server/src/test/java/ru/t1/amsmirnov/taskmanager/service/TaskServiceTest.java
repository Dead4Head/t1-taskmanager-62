package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IProjectDtoService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.ITaskDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.NameEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Category(DBCategory.class)
public class TaskServiceTest extends AbstractUserOwnedServiceTest {

    @NotNull
    private static final ProjectDTO projectAlfa = new ProjectDTO();

    @NotNull
    private final ITaskDtoService taskDtoService = context.getBean(ITaskDtoService.class);

    @NotNull
    private final IProjectDtoService projectDtoService = context.getBean(IProjectDtoService.class);

    @NotNull
    private List<TaskDTO> tasks;

    @NotNull
    private List<TaskDTO> alfaTasks;

    @NotNull
    private List<TaskDTO> betaTasks;


    @BeforeClass
    public static void initData() throws AbstractException {
        projectAlfa.setName("Alfa project");
        projectAlfa.setDescription("Alfa description");
        projectAlfa.setUserId(USER_ALFA_ID);
    }

    @Before
    public void initRepository() throws Exception {
        if (!projectDtoService.existById(projectAlfa.getId()))
                projectDtoService.add(projectAlfa);
        tasks = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("TaskDTO name: " + (NUMBER_OF_ENTRIES - i));
            task.setDescription("TaskDTO description: " + i);
            if (i <= 5) {
                task.setUserId(USER_ALFA_ID);
                task.setProjectId(projectAlfa.getId());
            } else
                task.setUserId(USER_BETA_ID);
            taskDtoService.add(task);
            tasks.add(task);
        }
        tasks = tasks.stream().sorted(TaskSort.BY_CREATED.getComparator()).collect(Collectors.toList());
        alfaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .sorted(TaskSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
        betaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .sorted(TaskSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() {
        taskDtoService.removeAll();
        projectDtoService.removeAll();
    }

    @Test
    public void testChangeTaskStatusById() throws AbstractException {
        int i = 0;
        for (final TaskDTO task : tasks) {
            if (i % 2 == 0) {
                taskDtoService.changeStatusById(task.getUserId(), task.getId(), Status.IN_PROGRESS);
                assertEquals(Status.IN_PROGRESS, taskDtoService.findOneById(task.getId()).getStatus());
                taskDtoService.changeStatusById(task.getUserId(), task.getId(), null);
                assertEquals(Status.IN_PROGRESS, taskDtoService.findOneById(task.getId()).getStatus());
            }
            if (i % 3 == 0) {
                taskDtoService.changeStatusById(task.getUserId(), task.getId(), Status.COMPLETED);
                assertEquals(Status.COMPLETED, taskDtoService.findOneById(task.getId()).getStatus());
            }
            i++;
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusById_IdEmptyException_1() throws AbstractException {
        taskDtoService.changeStatusById(NONE_STR, "", Status.IN_PROGRESS);
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusById_IdEmptyException_2() throws AbstractException {
        taskDtoService.changeStatusById(NONE_STR, NULL_STR, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusById_UserEmptyException_1() throws AbstractException {
        taskDtoService.changeStatusById("", NONE_STR, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusById_UserEmptyException_2() throws AbstractException {
        taskDtoService.changeStatusById(NULL_STR, NONE_STR, Status.IN_PROGRESS);
    }

    @Test
    public void testCreate() throws AbstractException {
        final String name = "TEST project";
        final String description = "Description";
        final TaskDTO task = taskDtoService.create(USER_ALFA_ID, name, description);
        final TaskDTO actualTask = taskDtoService.findOneById(task.getId());
        assertEquals(name, actualTask.getName());
        assertEquals(description, actualTask.getDescription());
        assertEquals(USER_ALFA_ID, actualTask.getUserId());
        assertEquals(taskDtoService.getSize(), tasks.size() + 1);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreate_NameException_1() throws AbstractException {
        final String description = "Description";
        taskDtoService.create(USER_ALFA_ID, "", description);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreate_NameException_2() throws AbstractException {
        final String description = "Description";
        taskDtoService.create(USER_ALFA_ID, NULL_STR, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreate_UserEmptyException_1() throws AbstractException {
        final String description = "Description";
        taskDtoService.create("", NONE_STR, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreate_UserEmptyException_2() throws AbstractException {
        final String description = "Description";
        taskDtoService.create(NULL_STR, NONE_STR, description);
    }

    @Test
    public void testFindAllByProjectId() throws AbstractException {
        final List<TaskDTO> alfaTasksByProject = alfaTasks
                .stream()
                .filter(t -> projectAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<TaskDTO> betaTasksByProject = betaTasks
                .stream()
                .filter(t -> projectAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<TaskDTO> alfaServTasks = taskDtoService.findAllByProjectId(USER_ALFA_ID, projectAlfa.getId());
        final List<TaskDTO> betaServTasks = taskDtoService.findAllByProjectId(USER_BETA_ID, projectAlfa.getId());
        assertNotEquals(alfaServTasks, betaServTasks);
        assertEquals(alfaTasksByProject, alfaServTasks);
        assertEquals(betaTasksByProject, betaServTasks);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectId_UserEmptyException_1() throws AbstractException {
        taskDtoService.findAllByProjectId("", projectAlfa.getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectId_UserEmptyException_2() throws AbstractException {
        taskDtoService.findAllByProjectId(NULL_STR, projectAlfa.getId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testFindAllByProjectId_ProjectEmptyException_1() throws AbstractException {
        taskDtoService.findAllByProjectId(NONE_STR, "");
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testFindAllByProjectId_ProjectEmptyException_2() throws AbstractException {
        taskDtoService.findAllByProjectId(NONE_STR, NULL_STR);
    }

    @Test
    public void testUpdateById() throws AbstractException {
        for (final TaskDTO task : tasks) {
            final String name = task.getName() + "TEST";
            final String description = task.getDescription() + "TEST";
            taskDtoService.updateById(task.getUserId(), task.getId(), name, description);
            assertEquals(name, taskDtoService.findOneById(task.getId()).getName());
            assertEquals(description, taskDtoService.findOneById(task.getId()).getDescription());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateById_UserEmptyException_1() throws AbstractException {
        taskDtoService.updateById("", NONE_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateById_UserEmptyException_2() throws AbstractException {
        taskDtoService.updateById(NULL_STR, NONE_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateById_IdException_1() throws AbstractException {
        taskDtoService.updateById(NONE_STR, "", NONE_STR, NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateById_IdException_2() throws AbstractException {
        taskDtoService.updateById(NONE_STR, NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateById_NameEmptyException_1() throws AbstractException {
        taskDtoService.updateById(NONE_STR, NONE_STR, "", NONE_STR);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateById_NameEmptyException_2() throws AbstractException {
        taskDtoService.updateById(NONE_STR, NONE_STR, NULL_STR, NONE_STR);
    }

}
