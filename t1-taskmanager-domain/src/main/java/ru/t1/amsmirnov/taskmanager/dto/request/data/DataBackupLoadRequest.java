package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest() {
    }

    public DataBackupLoadRequest(@NotNull final String token) {
        super(token);
    }

}
