package ru.t1.amsmirnov.taskmanager.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public class AbstractUserException extends AbstractException {

    public AbstractUserException() {

    }

    public AbstractUserException(@NotNull final String message) {
        super(message);
    }

    public AbstractUserException(
            @NotNull final String message,
            @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractUserException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractUserException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
